// 是否启用代理
const PROXY_ENABLE = true;

/// 代理服务IP
const PROXY_IP = '10.1.7.34';

/// 代理服务端口
const PROXY_PORT = 8866;
