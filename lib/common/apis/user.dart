
import 'package:flutter/material.dart';
import 'package:flutter_news/common/entitys/entitys.dart';
import 'package:flutter_news/common/utils/http.dart';

/// 用户
class UserAPI {
  /// 登录
  static Future<UserResponseEntity> login({UserRequestEntity params,@required BuildContext context,}) async {
    var response = await HttpUtil().post('/user/login', params: params,context: context);
    return UserResponseEntity.fromJson(response);
  }
}
